# WunderFleet

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.2.

## CORS

I've used `proxy.conf.json` to avoid CORS errors. Unfortunately provided route doesn't allow CORS. Also, the relevant option added `angular.json`

```"options": { "proxyConfig": "./proxy.conf.json" }```

## Possible optimizations and improvements
* Unit tests (Unfortunately I didn't write unit tests in my previous companies).
* Using NGRX instead of Input/Output could be clean but, it was only a small app.
* I've used Angular Material and override Angular Material classes. Creating a new theme with "Wunder Mobility" corporate identity could be better.

---

## Guide to run the project locally

1. Install dependencies `npm install`
2. Run app `ng serve`
3. Open the browser and enter this URL `http://localhost:4200/`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

---
## Screenshots

![Screenshot 1](screenshots/001.png)
![Screenshot 2](screenshots/002.png)
