import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  setItem(key: string, data: any, stringify = true) {
    localStorage.setItem(key, stringify ? JSON.stringify(data) : data);
  }

  getItem(key: string) {
    return JSON.parse(<string>localStorage.getItem(key));
  }

  clear() {
    localStorage.clear();
  }
}
