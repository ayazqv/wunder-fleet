import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaymentDataPost, PaymentDataResponse } from '../interfaces/registration.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  route = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data'

  constructor(private httpClient: HttpClient) { }

  sendPaymentData(data: PaymentDataPost) {
    // return this.httpClient.post<PaymentDataResponse>(this.route, data).toPromise()
    return this.httpClient.post<PaymentDataResponse>('/api', data).toPromise()
  }
}
