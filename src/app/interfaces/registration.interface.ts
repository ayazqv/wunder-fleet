export interface PersonalInformation {
  firstName: string
  lastName: string
  telephone: number
  valid?: boolean
}

export interface PaymentInformation {
  owner: string
  iban: string
  valid?: boolean
}

export interface Address {
  street: string
  houseNumber: string
  zip: number
  city: string
  valid?: boolean
}

export interface PaymentDataPost {
  customerId: number
  owner: string
  iban: string
}

export interface PaymentDataResponse {
  paymentDataId: string;
}
