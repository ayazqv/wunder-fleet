export enum LocalStorageData {
  PERSONAL_DATA = 'personalInformationData',
  PAYMENT_DATA = 'paymentInformationData',
  ADDRESS_DATA = 'addressInformationData',
  PAYMENT_DATA_ID = 'paymentDataId',
}
