import { Component, ViewChild, ChangeDetectionStrategy, AfterViewInit, ChangeDetectorRef } from '@angular/core'
import { Address, PaymentInformation, PersonalInformation } from '../interfaces/registration.interface'
import { LocalStorageService } from '../services/local-storage.service'
import { MatStepper } from '@angular/material/stepper';
import { LocalStorageData } from './registration.enum';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistrationComponent implements AfterViewInit {
  @ViewChild(MatStepper) stepper?: MatStepper;

  constructor(
    private localStorageService: LocalStorageService,
    private changeDetector: ChangeDetectorRef
  ) {}

  personalInformationData?: PersonalInformation
  paymentInformationData?: PaymentInformation
  addressInformationData?: Address
  allowEdit = true
  paymentDataId?: string
  errorMessage?: string

  ngAfterViewInit() {
    this.restoreDraftStep();
  }

  getPersonalInformationData(formData: PersonalInformation) {
    this.personalInformationData = formData
    this.localStorageService.setItem(LocalStorageData.PERSONAL_DATA, formData)
  }

  getPaymentInformationData(formData: PaymentInformation) {
    this.paymentInformationData = formData
    this.localStorageService.setItem(LocalStorageData.PAYMENT_DATA, formData)
  }

  getAddressInformationData(formData: Address) {
    this.addressInformationData = formData
    this.localStorageService.setItem(LocalStorageData.ADDRESS_DATA, formData)
  }

  restoreDraftStep() {
    const addressValidStatus = this.addressInformationData?.valid;
    const paymentValidStatus = this.paymentInformationData?.valid;
    const personalValidStatus = this.personalInformationData?.valid;

    if (addressValidStatus && this.isExistInLocalStorage(LocalStorageData.ADDRESS_DATA)) {
      this.move(2)
    } else if (paymentValidStatus && this.isExistInLocalStorage(LocalStorageData.PAYMENT_DATA)) {
      this.move(1)
    } else if (personalValidStatus && this.isExistInLocalStorage(LocalStorageData.PERSONAL_DATA)) {
      this.move(0)
    }
  }

  isExistInLocalStorage(key: string) {
    return this.localStorageService.getItem(key) !== null
  }

  move(index: number) {
    if (this.stepper) {
      this.stepper.linear = false
      this.stepper.selectedIndex = index
      this.stepper.linear = true
      this.changeDetector.detectChanges();
    }
  }

  disableStepEditing(status: boolean) {
    this.allowEdit = !status;
  }

  getPaymentDataId(dataId: string) {
    this.paymentDataId = dataId
  }

  getErrorDetails(errorMessage: string) {
    this.errorMessage = errorMessage
  }

}
