import { Component, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { LocalStorageService } from '../../../services/local-storage.service';
import { PersonalInformation } from '../../../interfaces/registration.interface';
import { LocalStorageData } from '../../registration.enum';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['../../registration.component.scss']
})
export class PersonalInformationComponent implements OnInit, OnDestroy {

  @Output() personalInformationStep = new EventEmitter();
  personalInformation: FormGroup;
  subscription?: Subscription;
  formData: PersonalInformation;

  constructor(private localStorageService: LocalStorageService) {
    this.formData = this.localStorageService.getItem(LocalStorageData.PERSONAL_DATA);
    this.personalInformation = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.subscription = this.personalInformation.valueChanges.subscribe(value => {
      const formData = { ...value, valid: this.personalInformation.valid }
      this.personalInformationStep.emit(formData)
    })

    if (this.formData) {
      this.personalInformation.setValue({
        firstName: this.formData.firstName,
        lastName: this.formData.lastName,
        telephone: this.formData.telephone
      })
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe()
  }
}
