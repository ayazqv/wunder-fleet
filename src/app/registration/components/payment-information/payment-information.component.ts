import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { LocalStorageService } from '../../../services/local-storage.service'
import { PaymentDataPost, PaymentInformation } from '../../../interfaces/registration.interface'
import { LocalStorageData } from '../../registration.enum';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-payment-information',
  templateUrl: './payment-information.component.html',
  styleUrls: ['../../registration.component.scss']
})
export class PaymentInformationComponent implements OnInit, OnDestroy {

  @Output() paymentInformationStep = new EventEmitter()
  @Output() editStepDisabled = new EventEmitter()
  @Output() paymentDataId = new EventEmitter()
  @Output() errorMessage = new EventEmitter()
  paymentInformation: FormGroup
  subscription?: Subscription
  formData: PaymentInformation
  customerId = Math.floor(Math.random() * 101)

  constructor(
    private localStorageService: LocalStorageService,
    private apiService: ApiService
  ) {
    this.formData = this.localStorageService.getItem(LocalStorageData.PAYMENT_DATA)
    this.paymentInformation = new FormGroup({
      owner: new FormControl('', Validators.required),
      iban: new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
    this.subscription = this.paymentInformation.valueChanges.subscribe(value => {
      const formData = { ...value, valid: this.paymentInformation.valid }
      this.paymentInformationStep.emit(formData)
    })

    if (this.formData) {
      this.paymentInformation.setValue({
        owner: this.formData.owner,
        iban: this.formData.iban
      })
    }
  }

  async sendPaymentData() {
    try {
      const body: PaymentDataPost = {
        customerId: this.customerId,
        owner: this.paymentInformation.controls.owner.value,
        iban: this.paymentInformation.controls.iban.value
      }
      await this.apiService.sendPaymentData(body).then((r) => {
        this.editStepDisabled.emit(true)
        this.paymentDataId.emit(r.paymentDataId)
        this.localStorageService.clear()
        this.localStorageService.setItem(JSON.stringify(this.customerId), r.paymentDataId)
      });
    } catch (e) {
      this.errorMessage.emit(e.statusText)
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe()
  }

}
