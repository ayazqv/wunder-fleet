import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { LocalStorageService } from '../../../services/local-storage.service';
import { Address } from '../../../interfaces/registration.interface';
import { LocalStorageData } from '../../registration.enum';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['../../registration.component.scss']
})
export class AddressComponent implements OnInit, OnDestroy {

  @Output() addressInformationStep = new EventEmitter();
  addressInformation: FormGroup
  subscription?: Subscription
  formData: Address

  constructor(private localStorageService: LocalStorageService) {
    this.formData = this.localStorageService.getItem(LocalStorageData.ADDRESS_DATA)
    this.addressInformation = new FormGroup({
      street: new FormControl('', Validators.required),
      houseNumber: new FormControl('', Validators.required),
      zip: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.subscription = this.addressInformation.valueChanges.subscribe(value => {
      const formData = { ...value, valid: this.addressInformation.valid }
      this.addressInformationStep.emit(formData)
    })

    if (this.formData) {
      this.addressInformation.setValue({
        street: this.formData.street,
        houseNumber: this.formData.houseNumber,
        zip: this.formData.zip,
        city: this.formData.city
      })
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe()
  }

}
